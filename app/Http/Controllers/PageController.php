<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function about() {
//		echo 'page - about';

        $user = User::all();

        //var_dump($user[0]->name);

        return view('page.about',
            [
                'name_of_user' => $user[0]->name
            ]
        );
	}
}
