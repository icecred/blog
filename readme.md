## How to install

$ git clone git@gitlab.com:icecred/blog.git <br>
$ cd blog <br>
$ composer install # (this will create 'vendor' folder and download all packages) <br>
create database <br>
copy .env.example and create .env file <br>
update database info in .env file <br>
$ php artisan migrate <br>

## About this project

This is an open-source project, created for education purpose.
